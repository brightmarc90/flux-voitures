$(document).ready((e) => {

    let carsList = $("main .cars-list");
    let loadBtn = $("main .load-flux")
    
    loadBtn.on("click", () => {
        fetch("./voitures.json")
        .then(response => response.json())
        .then(response => {
            response.forEach(element => {
                const li = document.createElement("li")
                    const figure = document.createElement("figure")
                        $(figure).append(`<img src="./asset/${element.image}" alt="${element.description}" />`)
                        const figcaption = document.createElement("figcaption")
                            $(figcaption).append(`<p><strong>Marque:</strong> ${element.marque}</p>`)
                            $(figcaption).append(`<p><strong>Modèle:</strong> ${element.type}</p>`)
                            $(figcaption).append(`<p><strong>Année:</strong> ${element.annee}</p>`)
                            $(figcaption).append(`<p><strong>Etat:</strong> ${element.etat}</p>`)
                            $(figcaption).append(`<p>${element.description}</p>`)
                    $(figcaption).appendTo(figure)
                $(figure).appendTo(li)
                $(li).appendTo(carsList)
            });
        });
        loadBtn.attr("disabled", true);
        loadBtn.text("Evénement désactivé");
    })  
})